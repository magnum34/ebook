#-*- coding: UTF-8 -*-
from django.db import models
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from phonenumber_field.modelfields import PhoneNumberField
from django.db.models.signals import post_save
from django.dispatch import receiver


class UserProfil(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE, primary_key=True)
    address = models.CharField(null=True,blank=True,max_length=150,verbose_name="Adres ")
    city = models.CharField(null=True,blank=True,max_length=100,verbose_name="Miasto")
    zip_code = models.CharField(null=True,blank=True,max_length=7,verbose_name="Kod pocztowy")
    phone = PhoneNumberField(null=True,blank=True,verbose_name="Telefon")

    def __unicode__(self):
        if self.user.username:
            return self.user.username
        else:
            return "Brak"

User.profil = property(lambda u: UserProfil.objects.get_or_create(user=u)[0])

#Genearate token superuser
for  user in User.objects.all():
        Token.objects.get_or_create(user=user)

@receiver(post_save,sender=User)
def add_user_profile(sender,**kwargs):
    if kwargs.get('created',False):
        UserProfil.objects.create(user=kwargs.get('instance'))



