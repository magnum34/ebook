#-*- coding: UTF-8 -*-
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Sum
from eBookApp.models.Book import Book
from utils.pricePost import data_price_post


class Payment(models.Model):
    name = models.CharField(max_length="70",verbose_name='Typ płatności')
    def __unicode__(self):
        return self.name

class PostType(models.Model):
    name = models.CharField(max_length="70", verbose_name='Forma przesyłki')
    def __unicode__(self):
        return self.name

class StatusOrder(models.Model):
    description = models.CharField(max_length="70", verbose_name='opis statusu zamówienia')
    code = models.IntegerField(verbose_name="kod statusu")
    def __unicode__(self):
        return self.description

class Order(models.Model):
    invoice_number = models.CharField(null=True,max_length="70", verbose_name='Numer Faktury')
    date = models.DateField(verbose_name="Data zamówienia",auto_now_add=True)
    user = models.ManyToManyField(User)
    post_type = models.ForeignKey(PostType)
    payment = models.ForeignKey(Payment)
    status = models.ForeignKey(StatusOrder)
    accept_bill = models.BooleanField(verbose_name="Potwierdzenie zapłaty produktu")

    def price_post(self):
        subtotal = 0
        for item in data_price_post():
            if item['payment_id'] == self.payment.id and item['posttype_id'] == self.post_type.id:
                subtotal = item['price']
        return subtotal
    def subtotal(self):
        items = OrderItem.objects.filter(order_main=self.id)
        total = sum(product.price * product.quantity for product in items)
        return total
    def total(self):
        items = OrderItem.objects.filter(order_main=self.id)
        total = sum(product.price * product.quantity for product in items) + self.price_post()
        return total
    def __unicode__(self):
        return " %s "%(self.invoice_number)

class OrderItem(models.Model):
    product = models.ForeignKey(Book)
    quantity = models.IntegerField(verbose_name="Ilość")
    price = models.DecimalField(max_digits=8,decimal_places=2,verbose_name="Cena")
    order_main = models.ForeignKey(Order)
    def __unicode__(self):
        return "Nr. %s, status - %s "%(self.order_main.invoice_number,self.order_main.status.description)






