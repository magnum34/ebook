#-*- coding: UTF-8 -*-
import hashlib
import json

from django.contrib import auth
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, Group
from django.core.context_processors import csrf
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse ,HttpResponseNotFound
from django.shortcuts import render
from eBook.core.payment.selectionPayment import PaymentAbstract
from eBook.core.payment.OrderAccept import OrderAccept
from eBook.core.integration.provider.OrderAcceptIntegration import OrderAcceptIntegration
from eBook.core.payment.Invoice import generatePDF
from UserApp.forms.ChangePasswordForm import ChangePasswordForm
from UserApp.models.Order import  Order, Payment, PostType
from UserApp.forms.UserForm import UserForm
from UserApp.forms.CreditCardForm import CreditCardForm
from UserApp.models.UserProfil import UserProfil
from UserApp.forms.UserProfilForm import  UserProfilForm, UserProfilOrderForm
from utils.cart import Cart
from utils.pricePost import data_price_post
from reportlab.pdfgen import canvas
from eBook.core import integration
from eBook.core.integration.decorator.custom_auth import custom_auth



def editDataProfil(profil):
    data = {
        'first_name': profil.user.first_name,
        'last_name': profil.user.last_name,
        'email': profil.user.email,
        'address': profil.address,
        'city': profil.city,
        'zip_code': profil.zip_code,
        'phone': profil.phone,
    }
    return data


def SignIn(request):
    context = {}
    context.update(csrf(request))
    return render(request,'signIn.html',context)

def auth_user(request):
    if request.method == "POST":
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        if integration.providerUserAuth(request,username=username,password=password):
            #auth.login(request, user)
            return HttpResponseRedirect('/account/')
        else:
            messages.warning(request, "Nie można się zalogować, podaj prawidłowe dane")
            return HttpResponseRedirect('/signIn/')

        # password = hashlib.sha1(request.POST.get('password', '')).hexdigest()
        # user = auth.authenticate(username=username, password=password)
        # if user is not None:
        #     auth.login(request, user)
        #     return HttpResponseRedirect('/account/')
        # else:


def SignOut(request):
    provider = integration.providerUser(request)
    provider.logout()
    #auth.logout(request)
    messages.success(request, "Wylogowałeś się")
    return HttpResponseRedirect('/')

def SignUp(request):
    form = UserForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            messages.success(request,"Zerejstrowano ciebie na eBook")
            user = form.save()
            user.groups.add(Group.objects.get(name='client'))
            return HttpResponseRedirect('/')
    context = {'form':form}
    context.update(csrf(request))
    return render(request,'signUp.html',context)

@custom_auth
def profil(request):
    provider = integration.providerUser(request)
    user = provider.GetUser()
    #user = User.objects.get(username=request.user.username)
    context = {'user':user}
    return render(request, 'profil.html', context)

@custom_auth
def changePassword(request):
    provider = integration.providerUser(request)
    form = provider.ChangePasswordForm()
    #form = ChangePasswordForm(data=request.POST or None, user=request.user)
    if request.method == 'POST':
        if form.is_valid():
            provider.saveChangePassword(form)
            messages.success(request, "Zmieniono hasło użytkownika")
            return HttpResponseRedirect('/')
    context = {'form':form}
    context.update(csrf(request))
    return render(request, 'changePassword.html', context)

@custom_auth
def profilAddress(request):
    provider = integration.providerUser(request)
    form = provider.GetUserProfilForm()
    #profil = UserProfil.objects.get(user__username=request.user.username)
    #data = editDataProfil(profil)
    #form = UserProfilForm(request.POST or None, initial=data,instance=request.user.profil)
    if request.method == "POST":
        if form.is_valid():
            provider.saveUserProfil(form)
            messages.success(request, "Zmieniono dane adresowe")
            return HttpResponseRedirect('/account/')
    context = {'form':form}
    context.update(csrf(request))
    return render(request, 'profilAddress.html', context)

@custom_auth
def listOrders(request):
    provider = integration.providerUser(request)
    orders = provider.GetListOrders()
    #orders = Order.objects.filter(user=request.user)
    context = {'orders':orders}
    return render(request, 'listOrders.html', context)

@custom_auth
def invoice(request,invoice_id):
    provider = integration.providerUser(request)
    response = None
    try:
        invoice = provider.GetOrder(invoice_id)
        user = provider.GetUser()
        data = {'user':user,'order':invoice}
        response = generatePDF(invoice.order_id,data)
    except Exception:
        return HttpResponse(status=404)
    return response

#----------------------------Order-----------------------------------
@custom_auth
def step1(request):
    provider = integration.providerUser(request)
    form = provider.GetUserProfilForm()
    #profil = UserProfil.objects.get(user__username=request.user.username)
    #data = editDataProfil(profil)
    #form = UserProfilOrderForm(request.POST or None, initial=data, instance=request.user.profil)
    if request.method == "POST":
        if form.is_valid():
            provider.saveUserProfil(form)
            # form.save()
            # user = User.objects.get(username=request.user.username)
            # user.first_name = request.POST.get('first_name')
            # user.last_name = request.POST.get('last_name')
            # user.email = request.POST.get('email')
            # user.save()
            messages.success(request, "Zmieniono dane adresowe")
            return HttpResponseRedirect(reverse('step2'))
    context = {'form': form}
    context.update(csrf(request))
    return render(request, 'cartOrder/step1.html', context)
@custom_auth
def step2(request):
    integration.providerUser(request)
    session = PaymentAbstract()
    session.clear(request)
    cart = Cart(request.session)
    payment = Payment.objects.all()
    post = PostType.objects.all()
    price = json.dumps(data_price_post())
    context = {'cart': cart,'payment':payment,'post':post,'price':price}
    context.update(csrf(request))
    return render(request, 'cartOrder/step2.html', context)

@custom_auth
def step3(request):
    integration.providerUser(request)
    if request.method == 'POST':
        payment = request.POST.get('payment')
        post = request.POST.get('post')
        choose = PaymentAbstract.factory(request,payment,post)
        return choose.load()
    return HttpResponseRedirect(reverse('step2'))


@custom_auth
def credit_card(request):
    integration.providerUser(request)
    form = CreditCardForm(request.POST or None)
    PaymentAbs = PaymentAbstract()
    provider = PaymentAbs.provider(request)
    if request.method == 'POST':
        if form.is_valid():
            #TODO integration provider PayLane and validation
            return HttpResponseRedirect(reverse('step_accept'))

    context = {'form':form}
    return render(request, 'cartOrder/creditCard.html', context)

@custom_auth
def step_accept(request):
    provider = integration.providerUser(request)
    PaymentAbs = PaymentAbstract()
    provider_payment = PaymentAbs.provider(request)
    if request.method == 'POST':
        #TODO:  Integration with eTechn

        #user = User.objects.get(username=request.user.username)
        cart = Cart(request.session)
        order = provider.SaveOrder(cart,provider_payment)
        if order:
            messages.success(request, "Zamówienie zostało zrealizowane")
            return HttpResponseRedirect(reverse('profil'))
        else:
            messages.warning(request, "Nie można realizować zamówiemia, spróbój ponownie")
            #return HttpResponseRedirect(reverse('step_accept'))
    payment = provider_payment.getPayment
    post = provider_payment.getPost
    price = provider_payment.price
    cart = Cart(request.session)
    total = cart.total + price
    user = provider.GetUser()
    context = {'cart': cart,'user':user,'payment': payment,'post':post, 'price': price,'total':total}
    context.update(csrf(request))
    return render(request, 'cartOrder/step3.html', context)

