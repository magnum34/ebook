#-*- coding: UTF-8 -*-
from django.utils.translation import ugettext as _
from django.contrib.auth.forms import PasswordChangeForm
from django.core.exceptions import ValidationError
import hashlib
from django import forms
from  collections import OrderedDict




def get_size_password():
    SIZE = 5
    return SIZE

class ChangePasswordForm(PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        style = 'form-control'
        new_fields = OrderedDict()
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
        self.fields['new_password2'].widget.attrs.update({'class': style, 'placeholder': 'Nowe hasło(powtórz'})
        self.fields['new_password1'].widget.attrs.update({'class': style, 'placeholder': 'Nowe hasło'})
        self.fields['old_password'].widget.attrs.update({'class': style, 'placeholder': 'Stare hasło'})
        new_fields['old_password'] = self.fields['old_password']
        new_fields['new_password1'] = self.fields['new_password1']
        new_fields['new_password2'] =self.fields['new_password2']
        self.fields = new_fields


    def clean_new_password1(self):
        password = self.cleaned_data.get('new_password1')
        if len(password) <= get_size_password():
            raise ValidationError('Hasło jest za krótkie')
        return password

    def clean_old_password(self):
        old_password = hashlib.sha1(self.cleaned_data["old_password"]).hexdigest()
        if not self.user.check_password(old_password):
            raise forms.ValidationError(
                self.error_messages['password_incorrect'],
                code='password_incorrect',
            )
        return old_password

    def save(self, commit=True):
        user = super(ChangePasswordForm, self).save(commit=False)
        user.set_password(hashlib.sha1(self.cleaned_data["new_password1"]).hexdigest())
        if commit:
            user.save()
        return user

#-----------eTechn

class ChangePasswordFormTechn(forms.Form):
    old_password = forms.CharField(label=_("Old password"),widget=forms.PasswordInput(attrs={'autofocus': True}),)
    new_password1 = forms.CharField(label=_("New password"),widget=forms.PasswordInput,)
    new_password2 = forms.CharField(label=_("New password confirmation"),widget=forms.PasswordInput,)
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
        'password_incorrect': _("Your old password was entered incorrectly. Please enter it again."),
    }
    def __init__(self,user=None, *args, **kwargs):
        style = 'form-control'
        self.user = user
        super(ChangePasswordFormTechn, self).__init__(*args, **kwargs)
        self.fields['old_password'].widget.attrs.update({'class': style, 'placeholder': 'Stare hasło'})
        self.fields['new_password1'].widget.attrs.update({'class': style, 'placeholder': 'Nowe hasło'})
        self.fields['new_password2'].widget.attrs.update({'class': style, 'placeholder': 'Nowe hasło(powtórz)'})
    def clean_new_password1(self):
        password = self.cleaned_data.get('new_password1')
        if len(password) <= get_size_password():
            raise ValidationError('Hasło jest za krótkie')
        return password

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
    def clean_old_password(self):
        old_password = hashlib.md5(self.cleaned_data["old_password"]).hexdigest()
        if self.user.password.lower() != old_password:
            raise forms.ValidationError(
                self.error_messages['password_incorrect'],
                code='password_incorrect',
            )
            return old_password

    def save(self, request):
        data = dict()
        data["Password"] = request.POST.get("new_password1")
        data["login"] = self.user.username
        return data