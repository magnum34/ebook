#-*- coding: UTF-8 -*-

import hashlib

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from UserApp.forms.ChangePasswordForm import get_size_password


class UserForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        style =  'form-control'
        super(UserForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'class': style,'placeholder':'Login'})
        self.fields['first_name'].widget.attrs.update({'class': style,'placeholder':'Imię'})
        self.fields['first_name'].required = True
        self.fields['last_name'].widget.attrs.update({'class': style,'placeholder':'Nazwisko'})
        self.fields['last_name'].required = True
        self.fields['email'].widget.attrs.update({'class': style,'placeholder':'e-mail'})
        self.fields['email'].required = True
        self.fields['password1'].widget.attrs.update({'class': style,'placeholder':'Hasło'})
        self.fields['password2'].widget.attrs.update({'class': style,'placeholder':'Powtórz hasło'})
    def clean_password1(self):
        password = self.cleaned_data.get('password1')
        if len(password) <= get_size_password():
            raise ValidationError('Hasło jest za krótkie')
        return password
    def clean_email(self):
        email = self.cleaned_data.get('email')
        if User.objects.filter(email=email).exists():
            raise ValidationError("Taki e-mail już istnieje, wpisz inny")
        return email

    def save(self, commit=True):
        user = super(UserForm,self).save(commit=False)
        user.set_password(hashlib.sha1(self.cleaned_data["password1"]).hexdigest())
        user.is_superuser = False
        user.is_staff = True
        user.is_active = True
        if commit:
            user.save()
        return user
    class Meta:
        model = User
        fields = ('username','first_name','last_name','email','password1','password2',)
