#-*- coding: UTF-8 -*-


from django import forms
from UserApp.forms.fields.fields import CCExpField, VerificationValueField

class CreditCardForm(forms.Form):
    first_name = forms.CharField(label='Imię', max_length=30, required=True)
    last_name = forms.CharField(label='Nazwisko', max_length=150, required=True)
    card_number = forms.IntegerField(label='Numer karty',required=True)
    expiration = CCExpField(label='Data ważności',required=True)
    ccv_number = VerificationValueField(label='Kod CVV2/CVC2',required=True)
    def __init__(self, *args, **kwargs):
        super(CreditCardForm, self).__init__(*args, **kwargs)
        for name, field in self.fields.items():
            field.widget.attrs.update({'class': 'form-control'})
