#-*- coding: UTF-8 -*-

from django import forms

from UserApp.models.UserProfil import UserProfil


class UserProfilForm(forms.ModelForm):

    first_name = forms.CharField(label='Imię', max_length=30, required=True)
    last_name = forms.CharField(label='Nazwisko', max_length=150, required=True)
    email = forms.EmailField(label="e-mail",required=True)
    def __init__(self, *args, **kwargs):
        super(UserProfilForm, self).__init__(*args, **kwargs)
        for name, field in self.fields.items():
            field.widget.attrs.update({'class': 'form-control'})


    class Meta:
        model = UserProfil
        fields = ('first_name','last_name','email','address','city','zip_code','phone',)

class UserProfilFormTechn(forms.Form):

    first_name = forms.CharField(label='Imię', max_length=30, required=True)
    last_name = forms.CharField(label='Nazwisko', max_length=150, required=True)
    email = forms.EmailField(label="e-mail",required=True)
    address = forms.CharField(label='Adres', max_length=150, required=False)
    city = forms.CharField(label='Miasto', max_length=150, required=False)
    zip_code = forms.CharField(label='Kod pocztowy', max_length=150, required=False)
    phone = forms.IntegerField(label='Telefon', required=False)
    def __init__(self, *args, **kwargs):
        super(UserProfilFormTechn, self).__init__(*args, **kwargs)
        for name, field in self.fields.items():
            field.widget.attrs.update({'class': 'form-control'})

    def save(self,request):
        keys = ("FirstName","LastName","Mail","Address","City","ZipCode","Phone")
        data = dict()
        i = 0
        for name, field in self.fields.items():
            data[keys[i]] = request.POST.get(name)
            i += 1
        return data

class UserProfilOrderForm(UserProfilForm):
    def __init__(self, *args, **kwargs):
        super(UserProfilOrderForm, self).__init__(*args, **kwargs)
        for name, field in self.fields.items():
            field.required = True