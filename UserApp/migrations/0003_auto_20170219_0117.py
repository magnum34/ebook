# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('UserApp', '0002_auto_20170218_2149'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofil',
            name='address',
            field=models.CharField(max_length=150, null=True, verbose_name=b'Adres ', blank=True),
        ),
        migrations.AlterField(
            model_name='userprofil',
            name='city',
            field=models.CharField(max_length=100, null=True, verbose_name=b'Miasto', blank=True),
        ),
        migrations.AlterField(
            model_name='userprofil',
            name='phone',
            field=phonenumber_field.modelfields.PhoneNumberField(max_length=128, null=True, verbose_name=b'Telefon', blank=True),
        ),
        migrations.AlterField(
            model_name='userprofil',
            name='zip_code',
            field=models.CharField(max_length=7, null=True, verbose_name=b'Kod pocztowy', blank=True),
        ),
    ]
