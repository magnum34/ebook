# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('UserApp', '0003_auto_20170219_0117'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofil',
            name='id',
        ),
        migrations.AlterField(
            model_name='userprofil',
            name='user',
            field=models.OneToOneField(primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL),
        ),
    ]
