# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('UserApp', '0005_auto_20170220_1654'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='status',
            field=models.ForeignKey(default=1, to='UserApp.StatusOrder'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='order',
            name='date',
            field=models.DateField(auto_now_add=True, verbose_name=b'Data zam\xc3\xb3wienia'),
        ),
        migrations.AlterField(
            model_name='order',
            name='invoice_number',
            field=models.CharField(max_length=b'70', null=True, verbose_name=b'Numer Faktury'),
        ),
    ]
