# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('eBookApp', '0008_auto_20170217_1412'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('UserApp', '0004_auto_20170220_1651'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('invoice_number', models.CharField(max_length=b'70', verbose_name=b'Numer Faktury')),
                ('date', models.DateField(verbose_name=b'Data zam\xc3\xb3wienia')),
                ('accept_bill', models.BooleanField(verbose_name=b'Potwierdzenie zap\xc5\x82aty produktu')),
            ],
        ),
        migrations.CreateModel(
            name='OrderItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.IntegerField(verbose_name=b'Ilo\xc5\x9b\xc4\x87')),
                ('price', models.DecimalField(verbose_name=b'Cena', max_digits=8, decimal_places=2)),
                ('product', models.ManyToManyField(to='eBookApp.Book')),
            ],
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=b'70', verbose_name=b'Typ p\xc5\x82atno\xc5\x9bci')),
            ],
        ),
        migrations.CreateModel(
            name='PostType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=b'70', verbose_name=b'Forma przesy\xc5\x82ki')),
            ],
        ),
        migrations.CreateModel(
            name='StatusOrder',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=b'70', verbose_name=b'opis statusu zam\xc3\xb3wienia')),
                ('code', models.IntegerField(verbose_name=b'kod statusu')),
            ],
        ),
        migrations.AddField(
            model_name='order',
            name='order_item',
            field=models.ForeignKey(to='UserApp.OrderItem'),
        ),
        migrations.AddField(
            model_name='order',
            name='payment',
            field=models.ForeignKey(to='UserApp.Payment'),
        ),
        migrations.AddField(
            model_name='order',
            name='post_type',
            field=models.ForeignKey(to='UserApp.PostType'),
        ),
        migrations.AddField(
            model_name='order',
            name='user',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
        ),
    ]
