from django.conf.urls import include, url


urlpatterns = [
    url(r'change_password/', 'UserApp.views.changePassword', name='changePassword'),
    url(r'profil_address/', 'UserApp.views.profilAddress', name='profilAddress'),
    url(r'list_orders/', 'UserApp.views.listOrders', name='listOrders'),
    url(r'auth/', 'UserApp.views.auth_user', name='auth'),
    url(r'signOut/', 'UserApp.views.SignOut', name='signOut'),
    url(r'$', 'UserApp.views.profil', name='profil'),

]