from django.conf.urls import include, url

urlpatterns = [
    url(r'step1/', 'UserApp.views.step1', name='step1'),
    url(r'step2/', 'UserApp.views.step2', name='step2'),
    url(r'step3/credit_card/', 'UserApp.views.credit_card', name='credit_card'),
    url(r'step3/', 'UserApp.views.step3', name='step3'),
    url(r'invoice/pdf/(?P<invoice_id>\w+)', 'UserApp.views.invoice', name='pdf'),

    url(r'accept/', 'UserApp.views.step_accept', name='step_accept'),

]