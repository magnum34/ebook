from django.conf.urls import include, url
from django.contrib import admin
from django.conf import  settings
from django.conf.urls.static import static


urlpatterns = [

    url(r'^$', 'eBookApp.views.home', name='home'),
    url(r'^page/(?P<page>\d+)/$', 'eBookApp.views.home', name='home'),
    url(r'^details/(?P<book_id>\d+)/', 'eBookApp.views.details', name='details'),
    url(r'^account/', include('UserApp.urls.urls_account')),
    url(r'^order/', include('UserApp.urls.urls_order')),
    url(r'^signIn/', 'UserApp.views.SignIn', name='signIn'),
    url(r'^signUp/', 'UserApp.views.SignUp', name='signUp'),
    url(r'^search/', include('eBookApp.urls.urls_search')),
    url(r'^cart/', include('eBookApp.urls.urls_cart')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/',include('api.urls_main'),name='api'),
    url(r'^tinymce/', include('tinymce.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
