#-*- coding: UTF-8 -*-

from UserApp.models.Order import Payment, PostType, Order, OrderItem, StatusOrder
from eBookApp.models.Book import Book
from utils.pricePost import data_price_post
from django.db import IntegrityError, transaction
from django.contrib import messages



class OrderAccept(object):
    def __init__(self ,user ,cart ,provider):
        self.user = user
        self.cart = cart
        self.provider = provider

    def listCart(self,invoice):
        products = []
        for item in self.cart.items:
            book = Book.objects.get(id=item.product.pk)
            order = OrderItem.objects.create(quantity=item.quantity,price=item.price,order_main=invoice,product=book)
            book.quantity  -= item.quantity
            book.save()
            products.append(order)
        return products
    @property
    def newStatus(self):
        status = StatusOrder.objects.get(code=100)
        return status
    def mainOrder(self):
        order = Order()
        order.accept_bill = False
        order.status = self.newStatus
        order.payment = self.provider.getPayment
        order.post_type = self.provider.getPost
        order.invoice_number = self.increment_invoice_number()
        return order

    def increment_invoice_number(self):
        size = Order.objects.all().count()
        if size == 0:
            return 'M0001'
        last_invoice = Order.objects.all().order_by('id').last()
        invoice_no = last_invoice.invoice_number
        invoice_int = int(invoice_no.split('M')[-1])
        new_invoice_int = invoice_int + 1
        new_invoice_no = 'M' + str(new_invoice_int)
        return new_invoice_no
    def save(self):
        try:
            with transaction.atomic():
                order = self.mainOrder()
                order.save()
                order.user.add(self.user)
                products = self.listCart(order)
                self.cart.clear()
                return True
        except IntegrityError:
            return False





