#-*- coding: UTF-8 -*-


from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from pyinvoice.models import InvoiceInfo, ServiceProviderInfo, ClientInfo, Item

from UserApp.models.Order import OrderItem
from utils.PyInvoiceModyfication.SimpleInvoice import SimpleInvoiceMy


def generatePDF(invoice,data):
    user = data['user']
    order = data['order']
    doc = SimpleInvoiceMy('/tmp/%s.pdf'%(invoice))
    # Paid stamp, optional

    doc.invoice_info = InvoiceInfo(invoice,order.date )  # Invoice info, optional

    # Service Provider Info, optional
    doc.service_provider_info = ServiceProviderInfo(
        name='eBook',
        street="Morska 1/12",
        city='Warszawa',
        state='Mazowieckie',
        country='Polska',
        post_code='22-222',
    )
    # Client info, optional
    doc.client_info = ClientInfo(
        name= "%s %s"%(user.first_name,user.last_name) ,
        city=user.city,
        post_code=user.zip_code,
        email=user.email,


    )

    # Add Item
    #items = OrderItem.objects.filter(order_main=order)

    for item in order.products:
        doc.add_item(Item(item.name_product, 'Item desc', item.count, item.price))
    doc.set_total(order)



    doc.finish()
    fs = FileSystemStorage("/tmp")
    with fs.open("%s.pdf"%(invoice)) as pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="%s.pdf"'%(invoice)
        return response
    return  doc