#-*- coding: UTF-8 -*-
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.shortcuts import render
from utils.cart import Cart
from django.core.context_processors import csrf
from UserApp.models.Order import Payment, PostType, Order, OrderItem
from utils.pricePost import data_price_post



class PaymentAbstract(object):
    def __init__(self):
        self.payment = None
        self.post = None
    @staticmethod
    def factory(request,payment,post):
        payment = int(payment)
        post = int(post)
        if payment == 1:
            return PaymentCreditCard(request,payment,post)
        elif payment == 2:

            return PaymentCOD(request,payment,post)
        else:
            return None

    def provider(self,request):
        if self.session_key in request.session:
            dict = request.session[self.session_key]
            self.payment = dict['payment']
            self.post = dict['post']
            if self.payment == 1:
                return PaymentCreditCard(request, self.payment, self.post)
            elif self.payment == 2:
                return PaymentCOD(request, self.payment, self.post)
            else:
                return None
    def load(self,request):
        raise NotImplementedError
    @property
    def session_key(self):
        return 'payment'
    @property
    def price(self):
        data = data_price_post()
        for item in data:
            if item['payment_id'] == self.payment and item['posttype_id'] == self.post:
                return item['price']
    @property
    def getPayment(self):
        return Payment.objects.get(id=self.payment)

    @property
    def getPost(self):
        return PostType.objects.get(id=self.post)

    def clear(self,request):
        if self.session_key in request.session:
            del request.session[self.session_key]



class PaymentCOD(PaymentAbstract):
    def __init__(self,request,payment,post):
        self.payment = payment
        self.post = post
        self.request = request
        if self.session_key in self.request.session:
            dict = self.request.session[self.session_key]
            self.payment = dict['payment']
            self.post = dict['post']
    def load(self):
        self.update_session(self.request.session)
        return HttpResponseRedirect(reverse('step_accept'))
    def update_session(self,session):
        self.session = session
        self.session[self.session_key] = {'payment':self.payment,'post':self.post}




class PaymentCreditCard(PaymentAbstract):
    def __init__(self,request,payment,post):
        self.payment = payment
        self.post = post
        self.request = request
        if self.session_key in self.request.session:
            dict = self.request.session[self.session_key]
            print dict
            self.payment = dict['payment']
            self.post = dict['post']
    def load(self):
        self.update_session(self.request.session)
        return HttpResponseRedirect(reverse('credit_card'))
    def update_session(self,session):
        self.session = session
        self.session[self.session_key] = {'payment':self.payment,'post':self.post}
