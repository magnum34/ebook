# -*- coding: UTF-8 -*-
from eBook.core.integration.models.TypedProperty import TypedProperty
import datetime
from django.db.models import CharField, IntegerField, DateField
import weakref

class EntityUser(object):
    username = TypedProperty('username',unicode)
    first_name =  TypedProperty('first_name',unicode)
    last_name = TypedProperty('last_name',unicode)
    password = TypedProperty('password',unicode)
    city = TypedProperty('city',unicode)
    email = TypedProperty('email',unicode)
    address = TypedProperty('address',unicode)
    zip_code = TypedProperty('zip_code',unicode)
    phone = TypedProperty('phone',unicode)
    date_joined = TypedProperty('date_joined',datetime.date)
    is_active = TypedProperty('is_activate',bool)
    is_authenticated = TypedProperty('is_authenticated',bool)
    is_anonymous =  TypedProperty('is_anonymous',bool)

class EntityPayment(object):
    id = TypedProperty('id',int)
    code = TypedProperty('code',str)
    description = TypedProperty('description',unicode)

class EntityPost(object):
    id = TypedProperty('id',int)
    price = TypedProperty('price',float)
    description = TypedProperty('description',unicode)

class EntityStatus(object):
    id = TypedProperty('id',int)
    description = TypedProperty('description', unicode)



class EntityOrderItem(object):
    id = TypedProperty('id', unicode)
    name_product = TypedProperty('name_product',unicode)
    price = TypedProperty('price', float)
    count = TypedProperty('count',int)




class EntityOrder(object):
    order_id = TypedProperty('order_id',unicode)
    date = TypedProperty('date',datetime.date)
    payment = TypedProperty('payment',EntityPayment)
    status =TypedProperty('status',EntityStatus)
    post = TypedProperty('post',EntityPost)
    products = TypedProperty('products',list)
    accept_bill = TypedProperty('accept_bill',bool)
    total = TypedProperty('total',float)
    subtotal = TypedProperty('subtotal',float)

