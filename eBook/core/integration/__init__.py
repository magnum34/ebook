# -*- coding: UTF-8 -*-
import hashlib


from eBook.core.integration.auth.Auth_eTechn import auth_integration
from eBook.core.integration.provider.eBookUser import eBookUser
from eBook.core.integration.provider.eTechnUser import eTechnUser
from django.contrib import auth

SESSION_KEY = 'platform'

def providerUserAuth(request,username=None,password=None):
    provider = ('eBook','eTechn')
    password_hash = hashlib.sha1(password).hexdigest()
    user = auth.authenticate(username=username, password=password_hash)
    if user is not None:
        request.session[SESSION_KEY] = provider[0]
        obj = factory(provider[0],request)
        obj.login(request,user)
        return True
    else:
        if auth_integration.authenticate(username=username, password=password):
            auth_integration.update_session(request)
            request.session[SESSION_KEY] = provider[1]
            factory(provider[1],request)
            return True
        return False

def providerUser(request):
    if SESSION_KEY in request.session:
        provider = request.session[SESSION_KEY]
        return factory(provider,request)
    return None

def factory(type,request):
    if type == 'eBook':
        return eBookUser(request)
    if type == 'eTechn':
        return eTechnUser(request)

