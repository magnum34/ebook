# -*- coding: UTF-8 -*-
from django.contrib import auth
from django.contrib.auth.models import User

from UserApp.forms.ChangePasswordForm import ChangePasswordForm
from UserApp.forms.UserProfilForm import  UserProfilForm
from UserApp.models.Order import  Order
from UserApp.models.UserProfil import UserProfil
from eBook.core.integration.models.editProfil import editDataProfil
from eBook.core.integration.models.ConvertUser import ConvertUserBook, ConvertOrderBook, ConvertOrderTechn
from eBook.core.integration import eTechnURI
from eBook.core.integration.provider.eTechnUser import SendHTTP
from eBook.core.integration.provider.OrderAcceptIntegration import OrderAcceptIntegration
from rest_framework.authtoken.models import Token

class eBookUser(object):
    def __init__(self,request):
        self.request = request
        self.entity = ConvertUserBook()
        self.model_order =ConvertOrderBook()
        self.model_integration_order =ConvertOrderTechn()
        #Token: Admin eTechn
        self.message = SendHTTP("YWRtaW46YWRtaW4=")
    def login(self,request,user):
        auth.login(request,user)
    def logout(self):
        auth.logout(self.request)
    def GetUser(self):
        user = User.objects.get(username=self.request.user.username)
        return self.entity.GetUser(user)
    def GetUserProfil(self):
        return UserProfil.objects.get(user__username=self.request.user.username)
    def saveChangePassword(self,form):
        form.save()
    def saveUserProfil(self,form):
        form.save()
        user = User.objects.get(username=self.request.user.username)
        user.first_name = self.request.POST.get('first_name')
        user.last_name = self.request.POST.get('last_name')
        user.email = self.request.POST.get('email')
        user.save()
    def GetUserProfilForm(self):
        profil = UserProfil.objects.get(user__username=self.request.user.username)
        data = editDataProfil(profil)
        return UserProfilForm(self.request.POST or None, initial=data, instance=self.request.user.profil)
    def ChangePasswordForm(self):
        return ChangePasswordForm(data=self.request.POST or None, user=self.request.user)
    def GetOrder(self,order_id):
        invoice = None
        try:
            order =  Order.objects.get(invoice_number=order_id)
            invoice = self.model_order.GetOrder(order)
        except Exception:
            url = eTechnURI.order_integration_uri(order_id, "eBook")
            data = self.message.send(url=url[1], method=url[0])
            invoice = self.model_integration_order.GetOrder(data)
        return invoice
    def GetListOrders(self):
        orders = Order.objects.filter(user=self.request.user)
        url = eTechnURI.order_integration_list_user_uri(str(self.request.user.id),"eBook")
        data = self.message.send(url=url[1], method=url[0])
        collection = self.model_integration_order.GetOrders(data)
        return self.model_order.GetOrders(orders) + collection
    def SaveOrder(self,cart ,provider):
        data = OrderAcceptIntegration(self.request.user.id,cart,provider)
        try:
            order = data.load()
            order["platform"] = "eBook"
            url = eTechnURI.order_integration_create_uri()

            r =self.message.send(url=url[1], method=url[0],**order)
            return  True
        except Exception:
            return False


