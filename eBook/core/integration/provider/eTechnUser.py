# -*- coding: UTF-8 -*-
import hashlib

from django.contrib import auth
from django.contrib.auth.models import User

from UserApp.forms.ChangePasswordForm import ChangePasswordFormTechn
from UserApp.forms.UserProfilForm import  UserProfilFormTechn
from UserApp.models.Order import  Order
from UserApp.models.UserProfil import UserProfil
from eBook.core.integration import eTechnURI
from eBook.core.integration.models.editProfil import editDataProfilTechn


from eBook.core.integration.auth.Auth_eTechn import session_provider, session_clear
from eBook.core.integration.models.ConvertUser import ConvertUserTechn, ConvertOrderTechn
from eBook.core.integration.auth.Auth_eTechn import auth_integration
from eBook.core.integration.provider.OrderAcceptIntegration import OrderAcceptIntegration
import requests, json





class SendHTTP(object):
    def __init__(self,token):
        self.token = token
    def send(self,url,method=None,**data):
        headers = {'Authorization': 'Basic '+self.token}
        try:
            if method == 'GET':
                r = requests.get(url,headers=headers)
                return r.json()
            elif method == 'POST':
                headers['Content-Type'] = "application/json"
                data_json = json.dumps(data)
                r = requests.post(url, headers=headers,data=data_json)
                return r.json()
            elif method == 'PUT':
                headers['Content-Type'] = "application/json"
                data_json = json.dumps(data)
                r = requests.put(url,headers=headers,data=data_json)
                return r
            elif method == 'DELETE':
                r = requests.delete(url,headers=headers)
                return r.json()
        except requests.exceptions.RequestException as ex:
            print  ex.message
            return None
        except Exception as ex:
            print ex.message
            return None


class eTechnUser(object):
    def __init__(self,request):
        self.request = request
        self.model_user = ConvertUserTechn()
        self.model_order =ConvertOrderTechn()
        self.session = session_provider(request)
        self.request.user.is_authenticated = True
        self.message = SendHTTP(self.session['token'])
    def login(self,request,user):
        pass
    def logout(self):
        session_clear(self.request)
    def GetUser(self):
        url = eTechnURI.user_uri(self.session['user_id'])
        user = self.message.send(url=url[1],method=url[0])
        return self.model_user.GetUser(user)
    def GetUserProfil(self):
        url = eTechnURI.user_uri(self.session['user_id'])
        user = self.message.send(url=url[1], method=url[0])
        return self.model_user.GetUser(user)
    def saveChangePassword(self,form):
        data = form.save(self.request)
        text_plain = data["Password"]
        data["Password"] = hashlib.md5(data["Password"]).hexdigest().upper()
        data['userIdstr'] = self.session['user_id']
        url = eTechnURI.user_update_uri()
        r = self.message.send(url=url[1], method=url[0], **data)
        if auth_integration.authenticate(username=data["login"],password=text_plain):
            auth_integration.update_session(self.request)


    def saveUserProfil(self,form):
        data = form.save(self.request)
        data['userIdstr'] = self.session['user_id']
        url = eTechnURI.user_update_uri()
        r = self.message.send(url=url[1], method=url[0],**data)
    def GetUserProfilForm(self):
        url = eTechnURI.user_uri(self.session['user_id'])
        user = self.model_user.GetUser( self.message.send(url=url[1], method=url[0]))
        data = editDataProfilTechn(user)
        return UserProfilFormTechn(self.request.POST or None, initial=data)
    def ChangePasswordForm(self):
        user = self.GetUser()
        return ChangePasswordFormTechn(user=user,data=self.request.POST or None)
    def GetOrder(self,order_id):
        url = eTechnURI.order_uri(order_id)
        data = self.message.send(url=url[1], method=url[0])
        return self.model_order.GetOrder(data)
    def GetListOrders(self):
        url = eTechnURI.order_list_user_uri(self.session['user_id'])
        data =self.message.send(url=url[1], method=url[0])
        return  self.model_order.GetOrders(data)
    def SaveOrder(self,cart ,provider):
        data = OrderAcceptIntegration(self.session['user_id'], cart, provider)
        try:
            order = data.load()
            order["platform"] = "eTechn"
            url = eTechnURI.order_integration_create_uri()
            r = self.message.send(url=url[1], method=url[0], **order)
            return True
        except Exception:
            return False