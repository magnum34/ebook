#-*- coding: UTF-8 -*-

from UserApp.models.Order import Payment, PostType, Order, OrderItem, StatusOrder
from eBookApp.models.Book import Book
from utils.pricePost import data_price_post
from django.db import IntegrityError, transaction
from django.contrib import messages
from datetime import datetime
from eBook.core.entity import EntityUser, EntityOrder, EntityPayment, EntityStatus, EntityPost, EntityOrderItem
from eBook.core.integration.auth.Auth_eTechn import session_provider, session_clear


class OrderAcceptIntegration(object):
    def __init__(self ,user_id,cart ,provider):
        self.user_id = user_id
        self.cart = cart
        self.provider = provider
        self.total = 0

    def listCart(self):
        products = []
        for item in self.cart.items:
            product = dict()
            book = Book.objects.get(id=item.product.pk)
            product["ProductId"] = item.product.pk
            product["ProductName"] = book.title
            product["ProductPlatform"] = "eBook"
            product["Count"] = item.quantity
            self.total += (item.quantity*item.price)
            product["Price"] = float(item.price)

            book.quantity  -= item.quantity
            book.save()
            products.append(product)
        return products
    @property
    def newStatus(self):
        status = StatusOrder.objects.get(code=100)
        return status
    def mainOrder(self):
        order = dict()
        order["userId"] = self.user_id
        order["Reservation"] = False
        order["AcceptBilling"] = False
        order["DateOrder"] = str(datetime.today())
        #Status
        status = self.newStatus
        status_dict = dict()
        status_dict["StatusId"] = status.id
        status_dict["Description"] = status.description
        order["StatusOrder"] = status_dict
        #--------Payment------------
        payment_dict = dict()
        payment = self.provider.getPayment
        payment_dict["PaymentId"] = payment.id
        if payment.id == 1:
             payment_dict["PaymentCode"] = "card"
        else:
            payment_dict["PaymentCode"] = "COD"
        payment_dict["Description"] = payment.name
        order["Payment"] = payment_dict
        #--------post-----
        post_dict = dict()
        post = self.provider.getPost
        post_dict["PostId"] = post.id
        post_dict["Description"] = post.name
        post_dict["Price"] =float(self.provider.price)
        order["TypePost"] = post_dict
        order["OrderProduct"] = self.listCart()
        order["Total"] = float(self.total)
        order["Sum"] = float(self.total + self.provider.price)
        return order

    def load(self):
        load = self.mainOrder()
        self.cart.clear()
        return load