# -*- coding: UTF-8 -*-
import requests, json
from eBook.core.integration import integration_settings , eTechnURI



SESSION_KEY = 'user_id'

def session_provider(request):
    if SESSION_KEY  in request.session.keys():
        return request.session[SESSION_KEY]
    return None

def session_clear(request):
    if SESSION_KEY  in request.session.keys():
        request.session[SESSION_KEY] = None
        del request.session[SESSION_KEY]


class ConfigHTTP(object):
    def __init__(self):
        self.host  =  integration_settings.HOST_NAME
        self.token = None
        self.user_id = None
    def getURL(self,name=None,dict=None):
        for item in dict:
            if name == item[0]:
                return self.host+item[2]
    def getMethod(self,name=None,dict=None):
        for item in dict:
            if name == item[0]:
                return item[1]

class AuthIntegration(ConfigHTTP):
    def __init__(self):
        ConfigHTTP.__init__(self)
    def authenticate(self,username=None,password=None):
        self.username = username
        self.password = password
        paths = eTechnURI.auth_uri
        url = self.getURL('auth',paths)
        if self.username is None or  self.password is None:
            return False
        try:
            data = { 'username': self.username, 'password':self.password}
            request = requests.post(url,data=data)
            data = request.json()
            self.token =  data['token']
            self.user_id = data['user_id']
            if self.token is None:
                return False
            return True
        except Exception as ex:
            return False
    def update_session(self,request):
        data_serializable = {
            'user_id': self.user_id,
            'token':self.token
        }
        request.session[SESSION_KEY] = data_serializable


auth_integration = AuthIntegration()