
def editDataProfil(profil):
    data = {
        'first_name': profil.user.first_name,
        'last_name': profil.user.last_name,
        'email': profil.user.email,
        'address': profil.address,
        'city': profil.city,
        'zip_code': profil.zip_code,
        'phone': profil.phone,
    }
    return data

def editDataProfilTechn(profil):
    data = {
        'first_name': profil.first_name,
        'last_name': profil.last_name,
        'email': profil.email,
        'address': profil.address,
        'city': profil.city,
        'zip_code': profil.zip_code,
        'phone': profil.phone,
    }
    return data