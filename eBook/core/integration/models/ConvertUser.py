# -*- coding: UTF-8 -*-
from django.contrib.auth.models import User

from UserApp.models.Order import OrderItem
from UserApp.models.UserProfil import UserProfil
from api.serializers.UserSerializer import UserSerializer
from eBook.core.entity import EntityUser, EntityOrder, EntityPayment, EntityStatus, EntityPost, EntityOrderItem
from django.db.models import CharField, IntegerField, DateField
from datetime import datetime


class ConvertUserTechn(object):
    def __init__(self):
        self.user = User()
        self.userProfil = UserProfil()
        self.model = EntityUser()
    def GetUser(self,data):
        self.model.username = unicode(data['login'])
        self.model.first_name = unicode(data['FirstName'])
        self.model.last_name = unicode(data['LastName'])
        self.model.password = unicode(data["Password"])
        self.model.city = unicode(data['City'])
        self.model.email = unicode(data['Mail'])
        self.model.address = unicode(data['Address'])
        self.model.phone = unicode(data['Phone'])
        self.model.zip_code = unicode(data['ZipCode'])
        self.model.is_active = bool(data['Enabled'])
        self.model.is_authenticated = True
        self.model.is_anonymous = False
        #self.model.date_joined = unicode(data['RegisterDate'])
        return self.model

class ConvertOrderTechn(object):
    def __init__(self):
        self.model = EntityOrder()
        self.tab = []
    def GetOrder(self,order):
        self.model = EntityOrder()
        self.model.order_id = unicode(order['orderId'])
        self.model.date = datetime.strptime(order['DateOrder'], '%Y-%m-%dT%H:%M:%S.%fZ')
        # payment
        payment = EntityPayment()
        payment.id = int(order['Payment']['PaymentId'])
        payment.name = str(order['Payment']['PaymentCode'])
        payment.description = unicode(order['Payment']['Description'])
        self.model.payment = payment
        # status
        status = EntityStatus()
        status.id = int(order['StatusOrder']['StatusId'])
        status.description = unicode(order['StatusOrder']['Description'])
        self.model.status = status
        # post
        post = EntityPost()
        post.id = int(order['TypePost']['PostId'])
        post.price = float(order['TypePost']['Price'])
        post.description = unicode(order['TypePost']['Description'])
        self.model.post = post
        # Products
        collection = []
        for product in order['OrderProduct']:
            prod = EntityOrderItem()
            prod.id = unicode(product['ProductId'])
            prod.name_product = unicode(product['ProductName'])
            prod.price = float(product['Price'])
            prod.count = int(product['Count'])
            collection.append(prod)
        self.model.products = collection
        self.model.accept_bill = bool(order['AcceptBilling'])
        self.model.subtotal = float(order['Total'])
        self.model.total = float(order['Sum'])
        return self.model
    def GetOrders(self,data):
        for item in data:
            self.GetOrder(item)
            self.tab.append(self.model)
        return self.tab



class ConvertUserBook(object):
    def __init__(self):
        self.model = EntityUser()
    def GetUser(self, user):
        self.model.username = (user.username)
        self.model.first_name = unicode(user.first_name)
        self.model.last_name = unicode(user.last_name)
        self.model.city = unicode(user.userprofil.city)
        self.model.email = unicode(user.email)
        self.model.address = unicode(user.userprofil.address)
        self.model.phone = unicode(user.userprofil.phone)
        self.model.zip_code = unicode(user.userprofil.zip_code)
        self.model.is_active = bool(user.is_active)
        self.model.is_authenticated = bool(user.is_authenticated)
        self.model.is_anonymous =  bool(user.is_anonymous )
        #print user.date_joined
        #self.model.date_joined = user.date_joined.date
        return self.model

class ConvertOrderBook(object):
    def __init__(self):
        self.model = EntityOrder()
        self.tab = []
    def GetOrder(self,order):
        self.model = EntityOrder()
        self.model.order_id = unicode(order.invoice_number)
        self.model.date = order.date
        # payment
        payment = EntityPayment()
        payment.id = int(order.payment.id)
        payment.name = str(order.payment.id)
        payment.description = unicode(order.payment.name)
        self.model.payment = payment
        # status
        status = EntityStatus()
        status.id = int(order.status.id)
        status.description = unicode(order.status.description)
        self.model.status = status
        # post
        post = EntityPost()
        post.id = int(order.post_type.id)
        post.price = float(order.price_post())
        post.description = unicode(order.post_type.name)
        self.model.post = post
        # Products
        collection = []
        products = OrderItem.objects.filter(order_main=order)
        for product in products:
            prod = EntityOrderItem()
            prod.id = unicode(product.id)
            prod.name_product = unicode(product.product.title)
            prod.price = float(product.price)
            prod.count = int(product.quantity)
            collection.append(prod)
        self.model.products = collection
        self.model.accept_bill = bool(order.accept_bill)
        self.model.subtotal = float(order.subtotal())
        self.model.total = float(order.total())
        return self.model
    def GetOrders(self,data):
        for item in data:
            self.GetOrder(item)
            self.tab.append(self.model)
        return self.tab