

class TypedProperty(object):
    def __init__(self,name,type,value=None):
        self.name = "_"+name
        self.type = type
        self.value = value

    def __get__(self, instance, cls):
        return getattr(instance,self.name,self.value)
    def __set__(self, instance, value):
        if not isinstance(value,self.type):
            raise TypeError("Must be a %s" % self.type)
        setattr(instance,self.name,value)

    def __delete__(self, instance):
        raise AttributeError("Can't delete attribute")