# -*- coding: UTF-8 -*-
from eBook.core.integration import integration_settings
METHOD  = ('GET','POST','PUT','DELETE')

auth_uri = [
    ('auth',METHOD[1],'/auth/token/')

]

def user_uri(user_id):
    url = integration_settings.HOST_NAME + '/user/'+user_id+'/'
    return (METHOD[0],url)
def user_create_uri():
    url = integration_settings.HOST_NAME + '/user/'
    return (METHOD[1],url)
def user_list_uri():
    url = integration_settings.HOST_NAME + '/user/'
    return (METHOD[0],url)
def user_update_uri():
    url = integration_settings.HOST_NAME + '/user/'
    return (METHOD[2],url)
def user_delete_uri(user_id):
    url = integration_settings.HOST_NAME + '/user/'+user_id+'/'
    return (METHOD[3],url)


def order_list_user_uri(user_id):
    url = integration_settings.HOST_NAME + '/order/user/' + user_id + '/'
    return (METHOD[0], url)


def order_update_uri(order_id):
    url = integration_settings.HOST_NAME + '/order/' + order_id + '/'
    return (METHOD[2], url)


def order_delete_uri(order_id):
    url = integration_settings.HOST_NAME + '/order/' + order_id + '/'
    return (METHOD[2], url)


def order_uri(order_id):
    url = integration_settings.HOST_NAME + '/order/' + order_id + '/'
    return (METHOD[0], url)

def order_create_uri():
    url = integration_settings.HOST_NAME + '/order/'
    return (METHOD[1], url)

def order_list_uri():
    url = integration_settings.HOST_NAME + '/order/'
    return (METHOD[0], url)

def order_integration_list_user_uri(user_id,platform):
    url = integration_settings.HOST_NAME + '/integration/order/list/'+user_id+"/"+platform+"/"
    return (METHOD[0], url)

def order_integration_uri(order_id,platform):
    url = integration_settings.HOST_NAME + '/integration/order/'+order_id+"/"+platform+"/"
    return (METHOD[0], url)

def order_integration_create_uri():
    url = integration_settings.HOST_NAME + '/integration/order/'
    return (METHOD[1], url)


