# -*- coding: UTF-8 -*-
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import auth
from django.contrib.auth.models import User

def custom_auth(f):
    def wrap(request, *args, **kwargs):
        if  'platform' not in request.session.keys():
            return HttpResponseRedirect('/signIn/')
        platform = request.session['platform']
        if platform == 'eBook':
            if '_auth_user_id' not in request.session.keys():
                 return HttpResponseRedirect('/signIn/')

            user_id = request.session['_auth_user_id']
            if not User.objects.filter(id=user_id).first():
                 return HttpResponseRedirect('/signIn/')
        if platform == 'eTechn':
            if 'user_id' not in request.session.keys():
                return HttpResponseRedirect('/signIn/')


        return f(request, *args, **kwargs)

    wrap.__doc__ = f.__doc__
    wrap.__name__ = f.__name__
    return wrap
