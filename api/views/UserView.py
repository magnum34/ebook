#-*- coding: UTF-8 -*-
import hashlib

from UserApp.models.UserProfil import  UserProfil
from django.contrib.auth.models import User
from rest_framework import generics
from rest_framework.pagination import PageNumberPagination
from django.template.defaultfilters import slugify
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from rest_framework import HTTP_HEADER_ENCODING, exceptions
from api.serializers.UserSerializer import UserSerializer, UserSerializerToken, UserUpdateSerializer, UserPasswordSerializer




class UserIdToken(APIView):
    queryset = User.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = UserSerializerToken
    keyword = "Token"
    def authenticate(self, request):
        auth = request.META.get('HTTP_AUTHORIZATION', b'')
        auth = auth.encode(HTTP_HEADER_ENCODING).split()
        if not auth or auth[0].lower() != self.keyword.lower().encode():
            return None

        if len(auth) == 1:
            msg = _('Invalid token header. No credentials provided.')
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = _('Invalid token header. Token string should not contain spaces.')
            raise exceptions.AuthenticationFailed(msg)

        try:
            token = auth[1].decode()
        except UnicodeError:
            msg = _('Invalid token header. Token string should not contain invalid characters.')
            raise exceptions.AuthenticationFailed(msg)
        return token
    def get(self, request, format=None):
        token = self.authenticate(request)
        user = Token.objects.get(key=token).user
        return Response({'id':user.id})

class UserPasswordAPI(APIView):
    queryset = User.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = UserPasswordSerializer
    lookup_field = 'id'
    lookup_url_kwarg = "user_id"
    def post(self,request,**kwargs):
        data = request.data
        try:
            user = User.objects.get(id=kwargs['user_id'])
            password = hashlib.sha1(data["password"]).hexdigest()
            if(user.check_password(password)):
                return Response({"check": True})
        except Exception:
            return Response({"check":False})
        return Response({"check":False})
    def put(self,request, user_id, format=None):
        data = request.data
        try:
            password = hashlib.sha1(data["password"]).hexdigest()
            user = User.objects.get(id=user_id)
            user.set_password(password)
            user.save()
            return Response({"status": True})
        except Exception:
            return Response({"status":False})

class UserListAPI(generics.ListAPIView):
    queryset =  User.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class =  UserSerializer

class UserDetailsAPI(generics.RetrieveAPIView):
    queryset =  User.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = UserSerializer
    lookup_field = 'id'
    lookup_url_kwarg = "user_id"

class UserCreateAPI(generics.CreateAPIView):
    queryset =  User.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = UserSerializer

class UserEditAPI(generics.UpdateAPIView):
    queryset =  User.objects.all()
    serializer_class = UserUpdateSerializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    lookup_field = 'id'
    lookup_url_kwarg = "user_id"

class UserDeleteAPI(generics.DestroyAPIView):
    queryset =  User.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = UserSerializer
    lookup_field = 'id'
    lookup_url_kwarg = "user_id"