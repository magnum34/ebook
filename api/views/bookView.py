#-*- coding: UTF-8 -*-
from eBookApp.models.Book import Book, get_upload_get
from eBookApp.models.Author import Author
from rest_framework import generics
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.pagination import PageNumberPagination
from api.serializers.bookSerializer import BookSerializer, BookCreateSerializer, BookUpdateSerializer, BookUpdateImageSerializer
from django.template.defaultfilters import slugify
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser
from django.db.models import Q
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication

class BookList(generics.ListAPIView):
    serializer_class = BookSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    filter_backends = [SearchFilter, OrderingFilter]
    pagination_class =  PageNumberPagination
    search_fields =  ['title','category','author','publisher']
    def get_queryset(self,*args,**kwargs):
        tags = {
            'title': '',
            'category': '',
            'author': '',
            'publisher':''
        }
        query_list =  Book.objects.all()
        object_filter = Q()
        for item_name in tags:
            if item_name in self.request.GET:
                value = slugify(self.request.GET.get(item_name))
                if item_name == 'title':
                    object_filter |= Q(slug__contains=value)
                elif item_name == 'category':
                    object_filter |= Q(category_id__slug__contains=value)
                elif item_name == "publisher":
                    object_filter |= Q(publisher_id__slug__contains=value)
                elif item_name == 'author':
                    authors = Author.objects.filter(slug__contains=value)
                    object_filter |= Q(author__in=authors)

        query_list = query_list.filter(
           object_filter
        ).distinct()
        return query_list

class BookDetails(generics.RetrieveAPIView):
    queryset = Book.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = BookSerializer
    lookup_field = 'id'
    lookup_url_kwarg = "book_id"

class BookCreateAPI(generics.CreateAPIView):
    queryset = Book.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = BookCreateSerializer

class BookEditAPI(generics.UpdateAPIView):
    queryset = Book.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = BookUpdateSerializer
    lookup_field = 'id'
    lookup_url_kwarg = "book_id"

class BookDeleteAPI(generics.DestroyAPIView):
    queryset = Book.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    parser_classes = (MultiPartParser, FormParser,)
    serializer_class = BookSerializer
    lookup_field = 'id'
    lookup_url_kwarg = "book_id"

class BookUpdateImageAPI(generics.UpdateAPIView):
    queryset = Book.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    parser_classes = (MultiPartParser, FormParser,)
    serializer_class = BookUpdateImageSerializer
    lookup_field = 'id'
    lookup_url_kwarg = "book_id"
"""
curl -X PUT -F "img=@"/home/serwer/Obrazy/test.jpg";type=image/jpg"  192.168.1.181:8000/api/books/update-image/7/
"""






