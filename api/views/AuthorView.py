#-*- coding: UTF-8 -*-
from eBookApp.models.Author import Author
from rest_framework import generics
from api.serializers.AuthorSerializer import AuthorSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
class AuthorListAPI(generics.ListAPIView):
    queryset =  Author.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class =  AuthorSerializer

class AuthorDetailsAPI(generics.RetrieveAPIView):
    queryset =  Author.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class =  AuthorSerializer
    lookup_field = 'id'
    lookup_url_kwarg = "author_id"

class AuthorCreateAPI(generics.CreateAPIView):
    queryset =  Author.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = AuthorSerializer

class AuthorEditAPI(generics.UpdateAPIView):
    queryset =  Author.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = AuthorSerializer
    lookup_field = 'id'
    lookup_url_kwarg = "author_id"

class AuthorDeleteAPI(generics.DestroyAPIView):
    queryset =  Author.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = AuthorSerializer
    lookup_field = 'id'
    lookup_url_kwarg = "author_id"