#-*- coding: UTF-8 -*-
from eBookApp.models.Category import Category
from rest_framework import generics
from api.serializers.CategorySerializer import CategorySerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication

class CategoryListAPI(generics.ListAPIView):
    queryset =  Category.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = CategorySerializer

class CategoryDetailsAPI(generics.RetrieveAPIView):
    queryset =  Category.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = CategorySerializer
    lookup_field = 'id'
    lookup_url_kwarg = "category_id"

class CategoryCreateAPI(generics.CreateAPIView):
    queryset =  Category.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = CategorySerializer

class CategoryEditAPI(generics.UpdateAPIView):
    queryset =  Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    lookup_field = 'id'
    lookup_url_kwarg = "category_id"

class CategoryDeleteAPI(generics.DestroyAPIView):
    queryset =  Category.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = CategorySerializer
    lookup_field = 'id'
    lookup_url_kwarg = "category_id"

