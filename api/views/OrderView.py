#-*- coding: UTF-8 -*-
from UserApp.models.Order import Order, OrderItem, StatusOrder
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from api.serializers.OrderSerializer import OrderSerializer
from rest_framework.views import APIView


class OrdersListAPI(generics.ListAPIView):
    queryset =  Order.objects.all()
    serializer_class = OrderSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

class OrdersDetailsAPI(generics.RetrieveAPIView):
    queryset =  Order.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = OrderSerializer
    lookup_field = 'invoice_number'
    lookup_url_kwarg = "order_id"

class OrdersListUserAPI(APIView):
    serializer_class = OrderSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    lookup_field = 'id'
    lookup_url_kwarg = "order_id"
    def get(self, request, *args, **kwargs):
        if 1 != len(kwargs.keys()):
            return None
        user_id = None
        for field in kwargs:
            user_id = kwargs[field]
        orders = Order.objects.filter(user=user_id)
        return Response(OrderSerializer(orders,many=True).data)

class OrdersDeleteAPI(generics.DestroyAPIView):
    queryset =  Order.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = OrderSerializer
    lookup_field = 'invoice_number'
    lookup_url_kwarg = "order_id"
