#-*- coding: UTF-8 -*-
from eBookApp.models.Publisher import Publisher
from rest_framework import generics
from api.serializers.PublisherSerializer import PublisherSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication

class PublisherListAPI(generics.ListAPIView):
    queryset =  Publisher.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = PublisherSerializer

class PublisherDetailsAPI(generics.RetrieveAPIView):
    queryset =  Publisher.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = PublisherSerializer
    lookup_field = 'id'
    lookup_url_kwarg = "publisher_id"

class PublisherCreateAPI(generics.CreateAPIView):
    queryset =  Publisher.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = PublisherSerializer

class PublisherEditAPI(generics.UpdateAPIView):
    queryset =  Publisher.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = PublisherSerializer
    lookup_field = 'id'
    lookup_url_kwarg = "publisher_id"

class PublisherDeleteAPI(generics.DestroyAPIView):
    queryset =  Publisher.objects.all()
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    serializer_class = PublisherSerializer
    lookup_field = 'id'
    lookup_url_kwarg = "publisher_id"