# -*- coding: UTF-8 -*-
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import  settings
from django.conf.urls.static import static
#from rest_framework.authtoken import views
from api.views.AuthToken import AuthToken

urlpatterns = [
    url(r'^books/', include('api.urls.urls_books'), name='api_books'),
    url(r'^categories/', include('api.urls.urls_categories'), name='api_categories'),
    url(r'^authors/', include('api.urls.urls_authors'), name='api_authors'),
    url(r'^publishers/', include('api.urls.urls_publishers'), name='api_publishers'),
    url(r'^users/', include('api.urls.urls_users'),name='api_users'),
    url(r'^orders/', include('api.urls.urls_orders'),name='api_orders'),
    url(r'^auth/token/', AuthToken.as_view()),

]