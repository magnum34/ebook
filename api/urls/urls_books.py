# -*- coding: UTF-8 -*-
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import  settings
from django.conf.urls.static import static
from api.views.bookView import BookList, BookDetails, BookCreateAPI, BookEditAPI, BookDeleteAPI, BookUpdateImageAPI

urlpatterns = [
    url(r'create/$', BookCreateAPI.as_view(), name='api_books:create'),
    url(r'edit/(?P<book_id>\d+)/$', BookEditAPI.as_view(), name='api_books:edit'),
    url(r'update-image/(?P<book_id>\d+)/$', BookUpdateImageAPI.as_view(), name='api_books:update-image'),
    url(r'delete/(?P<book_id>\d+)/$', BookDeleteAPI.as_view(), name='api_books:delete'),
    url(r'(?P<book_id>\d+)/$', BookDetails.as_view(), name='api_books:details'),
    url(r'$', BookList.as_view(), name='api_books:list'),

]