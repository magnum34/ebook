# -*- coding: UTF-8 -*-
from django.conf.urls import include, url
from django.conf.urls.static import static
from api.views.CategoryView import CategoryListAPI, CategoryDetailsAPI, CategoryCreateAPI, CategoryEditAPI, CategoryDeleteAPI

urlpatterns = [
    url(r'create/$', CategoryCreateAPI.as_view(), name='api_categories:create'),
    url(r'edit/(?P<category_id>\d+)/$', CategoryEditAPI.as_view(), name='api_categories:edit'),
    url(r'delete/(?P<category_id>\d+)/$', CategoryDeleteAPI.as_view(), name='api_categories:delete'),
    url(r'(?P<category_id>\d+)/$', CategoryDetailsAPI.as_view(), name='api_categories:details'),
    url(r'$', CategoryListAPI.as_view(), name='api_categories:list'),


]