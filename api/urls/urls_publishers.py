# -*- coding: UTF-8 -*-
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import  settings
from django.conf.urls.static import static
from api.views.PublisherView import PublisherListAPI, PublisherDetailsAPI, PublisherCreateAPI, PublisherEditAPI, PublisherDeleteAPI

urlpatterns = [
    url(r'create/$', PublisherCreateAPI.as_view(), name='api_publishers:create'),
    url(r'edit/(?P<publisher_id>\d+)/$', PublisherEditAPI.as_view(), name='api_publishers:edit'),
    url(r'delete/(?P<publisher_id>\d+)/$', PublisherDeleteAPI.as_view(), name='api_publishers:delete'),
    url(r'(?P<publisher_id>\d+)/$', PublisherDetailsAPI.as_view(), name='api_publisher:details'),
    url(r'$', PublisherListAPI.as_view(), name='api_categories:list'),


]