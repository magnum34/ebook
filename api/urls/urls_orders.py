# -*- coding: UTF-8 -*-
from django.conf.urls import include, url

from api.views.OrderView import OrdersListUserAPI, OrdersListAPI, OrdersDetailsAPI, OrdersDeleteAPI
urlpatterns = [

    url(r'user/(?P<order_id>\d+)/$', OrdersListUserAPI.as_view(), name='api_orders:user'),
    url(r'delete/(?P<order_id>\w+)/$', OrdersDeleteAPI.as_view(), name='api_orders:delete'),
    url(r'(?P<order_id>\w+)/$', OrdersDetailsAPI.as_view(), name='api_orders:details'),
    url(r'$', OrdersListAPI.as_view(), name='api_orders:list'),



]