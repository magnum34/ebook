# -*- coding: UTF-8 -*-
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import  settings
from django.conf.urls.static import static
from api.views.AuthorView import AuthorDetailsAPI, AuthorListAPI, AuthorCreateAPI, AuthorEditAPI, AuthorDeleteAPI

urlpatterns = [
    url(r'create/$', AuthorCreateAPI.as_view(), name='api_authors:create'),
    url(r'edit/(?P<author_id>\d+)/$', AuthorEditAPI.as_view(), name='api_authors:edit'),
    url(r'delete/(?P<author_id>\d+)/$', AuthorDeleteAPI.as_view(), name='api_authors:delete'),
    url(r'(?P<author_id>\d+)/$', AuthorDetailsAPI.as_view(), name='api_authors:details'),
    url(r'$', AuthorListAPI.as_view(), name='api_authors:list'),


]