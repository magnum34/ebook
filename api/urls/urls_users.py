# -*- coding: UTF-8 -*-
from django.conf.urls import include, url

from api.views.UserView import UserDetailsAPI, UserListAPI, UserEditAPI, UserDeleteAPI, UserCreateAPI, UserIdToken, UserPasswordAPI

urlpatterns = [
    url(r'create/$', UserCreateAPI.as_view(), name='api_users:create'),
    url(r'edit/(?P<user_id>\d+)/$', UserEditAPI.as_view(), name='api_users:edit'),
    url(r'delete/(?P<user_id>\d+)/$', UserDeleteAPI.as_view(), name='api_users:delete'),
    url(r'token/$', UserIdToken.as_view(), name='api_users:user_id'),
    url(r'password/(?P<user_id>\d+)/$', UserPasswordAPI.as_view(), name='api_users:password'),
    url(r'(?P<user_id>\d+)/$', UserDetailsAPI.as_view(), name='api_users:details'),
    url(r'$', UserListAPI.as_view(), name='api_users:list'),


]