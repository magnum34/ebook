#-*- coding: UTF-8 -*-
from eBookApp.models.Publisher import Publisher
from rest_framework.serializers import ModelSerializer, SerializerMethodField

class PublisherSerializer(ModelSerializer):
    class Meta:
        model = Publisher
        fields = [
            'id',
            'name_publisher',
            'slug'
        ]