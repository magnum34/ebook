#-*- coding: UTF-8 -*-
from eBookApp.models.Book import Book
from eBookApp.models.Category import  Category
from rest_framework.serializers import ModelSerializer, SerializerMethodField
from api.serializers.CategorySerializer import CategorySerializer
from api.serializers.PublisherSerializer import PublisherSerializer
from api.serializers.AuthorSerializer import AuthorSerializer
from api.serializers.AuthorSerializer import AuthorSerializer
from django.contrib.contenttypes.models import ContentType

class BookSerializer(ModelSerializer):
    img = SerializerMethodField()
    category = SerializerMethodField()
    publisher = SerializerMethodField()
    authors = SerializerMethodField()
    class Meta:
        model = Book
        fields = (
            'id',
            'title',
            'slug',
            'price',
            'quantity',
            'isbn',
            'published',
            'descriptions',
            'img',
            'category',
            'publisher',
            'authors'
        )
    def get_img(self,obj):
        try:
            image = obj.img.url
        except:
            image = None
        return image
    def get_category(self,obj):
        category =  obj.category_id
        return CategorySerializer(category,many=False).data
    def get_publisher(self,obj):
        publisher = obj.publisher_id
        return PublisherSerializer(publisher,many=False).data
    def get_authors(self,obj):
        authors = obj.author
        return AuthorSerializer(authors,many=True).data

class BookCreateSerializer(ModelSerializer):
    class Meta:
        model = Book
        fields = (
            'id',
            'title',
            'slug',
            'price',
            'quantity',
            'isbn',
            'published',
            'descriptions',
            'img',
            'category_id',
            'publisher_id',
            'author'
        )
class BookUpdateSerializer(ModelSerializer):
    class Meta:
        model = Book
        fields = (
            'id',
            'title',
            'slug',
            'price',
            'quantity',
            'isbn',
            'published',
            'descriptions',
            'category_id',
            'publisher_id',
            'author'
        )
class BookUpdateImageSerializer(ModelSerializer):
    class Meta:
        model = Book
        fields = (
            'id',
            'img'
        )

