#-*- coding: UTF-8 -*-
from UserApp.models.UserProfil import UserProfil
from django.contrib.auth.models import User

from UserApp.models.Order import  Order, OrderItem, StatusOrder, PostType, Payment
from rest_framework import  serializers
from eBookApp.models.Book import Book
from rest_framework.serializers import ModelSerializer, SerializerMethodField, HyperlinkedModelSerializer


class OrderItemSerializer(ModelSerializer):
    product_name  = SerializerMethodField()
    product_id = SerializerMethodField()
    class Meta:
        model = OrderItem
        fields = [
            'quantity',
            'price',
            'product_id',
            'product_name'
        ]
    def get_product_name(self,obj):
         return Book.objects.get(id=obj.product_id).title
    def get_product_id(self,obj):
        return obj.product_id

class PaymentSerializer(ModelSerializer):
    class Meta:
        model = Payment
        fields = [ 'id' , 'name'  ]


class PostTypeSerializer(ModelSerializer):
    class Meta:
        model = PostType
        fields = [ 'id', 'name']
class StatusOrderSerializer(ModelSerializer):
    class Meta:
        model = StatusOrder
        fields = [ 'id' , 'description', 'code']

class OrderSerializer(ModelSerializer):
    orderitem = SerializerMethodField()
    payment = SerializerMethodField()
    post = SerializerMethodField()
    post_price = SerializerMethodField()
    status = SerializerMethodField()
    class Meta:
        model = Order
        fields = [
            'invoice_number',
            'date',
            'accept_bill',
            'payment',
            'post',
            'post_price',
            'status',
            'orderitem'

        ]
    def get_orderitem(self,obj):
        item = OrderItem.objects.filter(order_main=obj.id)
        return OrderItemSerializer(item,many=True).data
    def get_payment(self,obj):
        return PaymentSerializer(obj.payment,many=False).data
    def get_post(self,obj):
        return PostTypeSerializer(obj.post_type,many=False).data
    def get_status(self,obj):
        return StatusOrderSerializer(obj.status,many=False).data
    def get_post_price(self,obj):
        return obj.price_post()