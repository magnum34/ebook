#-*- coding: UTF-8 -*-
from eBookApp.models.Category import  Category
from rest_framework.serializers import ModelSerializer, SerializerMethodField

class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = [
            'id',
            'name',
            'slug'
        ]