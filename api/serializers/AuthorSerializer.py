#-*- coding: UTF-8 -*-
from eBookApp.models.Author import  Author
from rest_framework.serializers import ModelSerializer, SerializerMethodField

class AuthorSerializer(ModelSerializer):
    class Meta:
        model = Author
        fields = [
            'id',
            'name',
            'slug'
        ]