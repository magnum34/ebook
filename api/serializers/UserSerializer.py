#-*- coding: UTF-8 -*-
from UserApp.models.UserProfil import UserProfil
from django.contrib.auth.models import User
from rest_framework import  serializers
from rest_framework.serializers import ModelSerializer, SerializerMethodField, HyperlinkedModelSerializer

class UserSerializer(HyperlinkedModelSerializer):
    address = serializers.CharField(source='userprofil.address')
    city = serializers.CharField(source='userprofil.city')
    zip_code = serializers.CharField(source='userprofil.zip_code')
    phone = serializers.CharField(source='userprofil.phone')
    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
            'address',
            'city',
            'zip_code',
            'phone'

        ]



class UserUpdateSerializer(HyperlinkedModelSerializer):
    address = serializers.CharField(source='userprofil.address',required=False)
    city = serializers.CharField(source='userprofil.city',required=False)
    zip_code = serializers.CharField(source='userprofil.zip_code',required=False)
    phone = serializers.CharField(source='userprofil.phone',required=False)
    class Meta:
        model = User
        fields = [
            'id',
            'first_name',
            'last_name',
            'email',
            'address',
            'city',
            'zip_code',
            'phone'

        ]

    def update(self, instance, validated_data):
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name',instance.last_name)
        instance.email =  validated_data.get('email',instance.email)
        instance.save()
        profile = instance.userprofil
        validated_data_profil = validated_data['userprofil']
        profile.address = validated_data_profil.get('address',profile.address)
        profile.city =  validated_data_profil.get('city',profile.city)
        profile.zip_code = validated_data_profil.get('zip_code', profile.zip_code)
        profile.phone = validated_data_profil.get('phone', profile.phone)

        profile.save()
        return instance

class UserPasswordSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = [ 'id','password']
class UserSerializerToken(ModelSerializer):
    class Meta:
        model = User
        fields = [ 'id']
