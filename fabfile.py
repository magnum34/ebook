from fabric.api import *
import os.path

env.hosts = ['localhost']
env.user = "serwer"

def access():
    sudo("chmod -R 755 /home/serwer/Django/eBook")
    sudo("chown www-data:www-data /home/serwer/Django/eBook/media")
    sudo("chown www-data:www-data /home/serwer/Django/eBook/static")
def change_ip():
    s = raw_input("IP =")
    fpath = os.path.join("/etc", "hosts")

    run("touch /tmp/hosts")
    tmp_file = open("/tmp/hosts","w+")
    with open(fpath, 'r') as f:
        lines = f.readlines()
        count = 0
        for line in lines:
            count += 1
            if "s1.ebook.pl" in line:
                change = s + " s1.ebook.pl\n"
                tmp_file.write(change)
            else:
                tmp_file.write(line)
    sudo("mv /tmp/hosts /etc/hosts")



def restart_webserver():
    '''Restart uwsgi '''
    sudo("cp /home/serwer/Django/eBook/ebook.ini /etc/uwsgi/apps-available/ebook.ini")
    if not os.path.isfile("/etc/uwsgi/apps-enabled/ebook.ini"):
        sudo("ln -s /etc/uwsgi/apps-available/ebook.ini /etc/uwsgi/apps-enabled/")
    sudo("service uwsgi restart")
    sudo("cp /home/serwer/Django/eBook/ebook-nginx /etc/nginx/sites-enabled/")
    sudo("service nginx restart")
def update():
    change_ip()
    restart_webserver()
