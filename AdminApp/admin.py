from eBookApp.models.Book import Book
from eBookApp.models.Publisher  import Publisher
from eBookApp.models.Category  import  Category
from eBookApp.models.Author import Author
from django import forms
from django.contrib import admin
from django.contrib.admin import ModelAdmin
from tinymce.widgets import TinyMCE

from UserApp.models.Order import PostType, Payment, StatusOrder, OrderItem, Order
from UserApp.models import UserProfil
from rest_framework.authtoken.admin import TokenAdmin



class PageForm(forms.ModelForm):
    class Meta:
        model = Book
        widgets = {
            'descriptions': TinyMCE()
        }
        fields = ('title', 'price','quantity','published','isbn','category_id','publisher_id','author','img','descriptions')

class PageAdmin(ModelAdmin):
    form = PageForm

    class Admin:
        js = ('js/tiny_mce/tiny_mce.js',
              'js/tiny_mce/textareas.js',
              )

admin.site.register(Category)
admin.site.register(Author)
admin.site.register(Publisher)
admin.site.register(Book, PageAdmin)
admin.site.register(UserProfil.UserProfil)
admin.site.register(Order)
admin.site.register(OrderItem)
admin.site.register(StatusOrder)
admin.site.register(PostType)
admin.site.register(Payment)

TokenAdmin.raw_id_fields = ('user',)




