from django import template
register = template.Library()
from utils.cart import Cart

@register.simple_tag
def cart_quantity(session):
    cart = Cart(session)
    return cart.count