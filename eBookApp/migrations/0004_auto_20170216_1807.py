# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('eBookApp', '0003_auto_20170216_1752'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='author',
            name='first_name',
        ),
        migrations.RemoveField(
            model_name='author',
            name='last_name',
        ),
        migrations.AddField(
            model_name='author',
            name='name',
            field=models.CharField(default=datetime.datetime(2017, 2, 16, 17, 7, 50, 707926, tzinfo=utc), max_length=b'255', verbose_name=b'Imi\xc4\x99 i Nazwisko: '),
            preserve_default=False,
        ),
    ]
