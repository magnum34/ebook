# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=b'60', verbose_name=b'Imi\xc4\x99: ')),
                ('last_name', models.CharField(max_length=b'60', verbose_name=b'Nazwisko')),
            ],
        ),
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'Tytu\xc5\x82: ')),
                ('price', models.DecimalField(verbose_name=b'Cena:', max_digits=8, decimal_places=2)),
                ('quantity', models.IntegerField(verbose_name=b'Ilo\xc5\x9b\xc4\x87:')),
                ('isbn', models.IntegerField(verbose_name=b'ISBN:')),
                ('published', models.DateField(verbose_name=b'data wydania:')),
                ('descriptions', models.TextField(verbose_name=b'Opis:')),
                ('img', models.ImageField(upload_to=b'books_images/', verbose_name=b'dodaj obrazek:')),
                ('author', models.ManyToManyField(to='eBookApp.Author')),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=b'70', verbose_name='Nazwa Kategorii:')),
            ],
        ),
        migrations.CreateModel(
            name='Publisher',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_publisher', models.CharField(max_length=b'70', verbose_name=b'Nazwa wydawnictwa:')),
            ],
        ),
        migrations.AddField(
            model_name='book',
            name='category_id',
            field=models.ForeignKey(to='eBookApp.Category'),
        ),
        migrations.AddField(
            model_name='book',
            name='publisher_id',
            field=models.ForeignKey(to='eBookApp.Publisher'),
        ),
    ]
