# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('eBookApp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='url',
            field=models.URLField(default=datetime.datetime(2017, 2, 16, 16, 26, 53, 263875, tzinfo=utc), verbose_name=b'url:'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='publisher',
            name='url',
            field=models.URLField(default=datetime.datetime(2017, 2, 16, 16, 27, 6, 643749, tzinfo=utc), verbose_name=b'url:'),
            preserve_default=False,
        ),
    ]
