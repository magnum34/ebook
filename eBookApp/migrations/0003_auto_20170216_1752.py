# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eBookApp', '0002_auto_20170216_1727'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='category',
            name='url',
        ),
        migrations.RemoveField(
            model_name='publisher',
            name='url',
        ),
    ]
