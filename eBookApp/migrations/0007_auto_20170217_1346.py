# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

import tinymce.models
from django.db import models, migrations
from django.utils.timezone import utc

import eBookApp.models.Book


class Migration(migrations.Migration):

    dependencies = [
        ('eBookApp', '0006_auto_20170216_1926'),
    ]

    operations = [
        migrations.AddField(
            model_name='author',
            name='slug',
            field=models.SlugField(default=datetime.datetime(2017, 2, 17, 12, 46, 12, 601614, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='category',
            name='slug',
            field=models.SlugField(default=datetime.datetime(2017, 2, 17, 12, 46, 22, 583060, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='publisher',
            name='slug',
            field=models.SlugField(default=datetime.datetime(2017, 2, 17, 12, 46, 43, 392484, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='book',
            name='descriptions',
            field=tinymce.models.HTMLField(null=True, verbose_name=b'Opis:', blank=True),
        ),
        migrations.AlterField(
            model_name='book',
            name='img',
            field=models.ImageField(upload_to=eBookApp.models.Book.get_upload_get, null=True, verbose_name=b'dodaj obrazek:', blank=True),
        ),
        migrations.AlterField(
            model_name='book',
            name='isbn',
            field=models.IntegerField(null=True, verbose_name=b'ISBN:', blank=True),
        ),
    ]
