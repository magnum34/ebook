# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('eBookApp', '0004_auto_20170216_1807'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='descriptions',
            field=tinymce.models.HTMLField(verbose_name=b'Opis:'),
        ),
    ]
