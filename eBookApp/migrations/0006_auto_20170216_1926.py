# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('eBookApp', '0005_auto_20170216_1901'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='descriptions',
            field=tinymce.models.HTMLField(null=True, verbose_name=b'Opis:'),
        ),
        migrations.AlterField(
            model_name='book',
            name='img',
            field=models.ImageField(upload_to=b'books_images/', null=True, verbose_name=b'dodaj obrazek:'),
        ),
        migrations.AlterField(
            model_name='book',
            name='isbn',
            field=models.IntegerField(null=True, verbose_name=b'ISBN:'),
        ),
    ]
