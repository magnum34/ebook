# -*- coding: utf-8 -*-
import os
import uuid

from eBookApp.models import Category, Publisher
from django.db import models
from django.template.defaultfilters import slugify
from tinymce.models import HTMLField
from eBookApp.models import Author


def get_upload_get(instance,filename):
    name = uuid.uuid4()
    file_name, file_type = filename.split(".")
    s = ""
    tab = (str(name),".",file_type)
    name = s.join(tab)
    return os.path.join("books_images",name)



class Book(models.Model):
    title = models.CharField(max_length=255,verbose_name="Tytuł: ")
    price = models.DecimalField(max_digits=8,decimal_places=2,verbose_name="Cena:")
    quantity = models.IntegerField(verbose_name="Ilość:")
    isbn = models.IntegerField(blank=True,null=True,verbose_name="ISBN:")
    published = models.DateField(auto_now=False,verbose_name="data wydania:")
    descriptions = HTMLField(blank=True,null=True,verbose_name="Opis:")
    category_id = models.ForeignKey(Category.Category)
    publisher_id = models.ForeignKey(Publisher.Publisher)
    author = models.ManyToManyField(Author.Author)
    img = models.ImageField(upload_to=get_upload_get,verbose_name="dodaj obrazek:",blank=True,null=True)
    slug = models.SlugField(blank=True)

    def save(self, *args,**kwargs):
        self.slug = slugify(self.title)
        super(Book,self).save(*args,**kwargs)
    def __unicode__(self):
        return self.title
    class Meta:
        app_label = "eBookApp"





