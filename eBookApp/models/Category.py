# -*- coding: utf-8 -*-
from django.db import models
from django.template.defaultfilters import slugify




class Category(models.Model):
    name = models.CharField(max_length="70",verbose_name='Nazwa Kategorii:')
    slug = models.SlugField(blank=True)
    def save(self, *args,**kwargs):
        self.slug = slugify(self.name)
        super(Category, self).save( *args, **kwargs)

    def __unicode__(self):
        return self.name
    class Meta:
        app_label = "eBookApp"