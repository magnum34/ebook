# -*- coding: utf-8 -*-
from django.db import models
from django.template.defaultfilters import slugify

class Publisher(models.Model):
    name_publisher = models.CharField(max_length="70",verbose_name="Nazwa wydawnictwa:")
    slug = models.SlugField(blank=True)
    def save(self, *args,**kwargs):
        self.slug = slugify(self.name_publisher)
        super(Publisher,self).save(*args,**kwargs)
    def __unicode__(self):
        return self.name_publisher
    class Meta:
        app_label = "eBookApp"