# -*- coding: utf-8 -*-
from django.db import models
from django.template.defaultfilters import slugify

class Author(models.Model):
    name = models.CharField(max_length="255",verbose_name="Imię i Nazwisko: ")
    slug = models.SlugField(blank=True)
    def save(self, *args,**kwargs):
        self.slug = slugify(self.name)
        super(Author,self).save(*args,**kwargs)
    def __unicode__(self):
        return self.name
    class Meta:
        app_label = "eBookApp"