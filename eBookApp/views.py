#-*- coding: UTF-8 -*-
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.template.defaultfilters import slugify
from django.http import Http404
from eBookApp.models.Book import Book
from utils.cart import Cart
from utils.pagination import pagination


def details(request,book_id):
    try:
        book = Book.objects.get(id=book_id)
    except Book.DoesNotExist:
        raise Http404("Taki adres nie istnieje")
    context = {'book':book}
    return render(request,'details.html',context)

def home(request,page=1):
    books = Book.objects.all()
    contacts = pagination(books,page)
    context = {'books': contacts,'url_page':('home',[''])}
    return render(request,'list.html',context)

def search_category(request,category_url, page=1):
    category_books = Book.objects.filter(category_id__slug=category_url).all()
    contacts = pagination(category_books, page)
    context = {'books': contacts,'url_page':('category',[category_url])}
    return render(request, 'list.html', context)

def search_publisher(request,publisher_url,page=1):
    publisher_books = Book.objects.filter(publisher_id__slug=publisher_url).all()
    contacts = pagination(publisher_books , page)
    context = {'books': contacts,'url_page':('publisher',[publisher_url])}
    return render(request, 'list.html', context)
def search_author(request,author_url,page=1):
    author_books = Book.objects.filter(author__slug=author_url).all()
    contacts = pagination(author_books, page)
    context = {'books': contacts, 'url_page': ('author', [author_url])}
    return render(request, 'list.html', context)

def search_form(request):
    if request.method == "GET":
        title = slugify(request.GET['title'])
        author =slugify(request.GET['author'])
        if not title:
            title = 'all'
        if not author:
            author = 'all'
        return HttpResponseRedirect(reverse('search', args=[title,author,1]))

def search(request,title,author,page=1):
    title_search = title
    author_search = author
    if title == 'all':
        title_search = ''
    if author == 'all':
        author_search = ''
    books = Book.objects.filter(slug__contains=title_search,author__slug__contains=author_search)
    contacts = pagination(books, page)
    context = {'books': contacts, 'url_page': ('search', [title,author])}
    return render(request, 'list.html', context)


#---------------Cart---------------------------
def add_cart(request,product_id):
    cart = Cart(request.session)
    product = Book.objects.get(id=product_id)
    cart.add(product,product.price)
    messages.success(request, "Dodano książkę do koszyka")
    return HttpResponseRedirect(reverse('showCart'))

def remove_cart(request,product_id):
    cart = Cart(request.session)
    product = Book.objects.get(id=product_id)
    cart.remove(product)
    messages.success(request, "Usunięto książkę z koszyka")
    return HttpResponseRedirect(reverse('showCart'))

def show_cart(request):
    cart = Cart(request.session)
    context = {'cart':cart}
    return render(request, 'cart.html', context)