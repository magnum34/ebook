from django.conf.urls import include, url



urlpatterns = [
    url(r'category/(?P<category_url>[\w\-]+)/(?P<page>\d+)/', 'eBookApp.views.search_category', name='category'),
    url(r'category/(?P<category_url>[\w\-]+)/', 'eBookApp.views.search_category', name='category'),

    url(r'publisher/(?P<publisher_url>[\w\-]+)/(?P<page>\d+)/', 'eBookApp.views.search_publisher', name='publisher'),
    url(r'publisher/(?P<publisher_url>[\w\-]+)/', 'eBookApp.views.search_publisher', name='publisher'),

    url(r'author/(?P<author_url>[\w\-]+)/(?P<page>\d+)/', 'eBookApp.views.search_author', name='author'),
    url(r'author/(?P<author_url>[\w\-]+)/', 'eBookApp.views.search_author', name='author'),

    url(r'(?P<title>[\w\-]+)/(?P<author>[\w\-]+)/page/(?P<page>\d+)/$', 'eBookApp.views.search', name='search'),
    url(r'$', 'eBookApp.views.search_form', name='search'),
]