from django.conf.urls import include, url



urlpatterns = [
    url(r'add/(?P<product_id>\d+)', 'eBookApp.views.add_cart', name='addCart'),
    url(r'remove/(?P<product_id>\d+)', 'eBookApp.views.remove_cart', name='removeCart'),
    url(r'show/', 'eBookApp.views.show_cart', name='showCart'),
]