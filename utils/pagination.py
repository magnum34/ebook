#-*- coding: UTF-8 -*-
from django.core.paginator import Paginator
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
PAGE_PER  = 5

def pagination(model, page):
    paginator = Paginator(model, PAGE_PER)
    try:
        contacts = paginator.page(page)
    except PageNotAnInteger:
        contacts = paginator.page(1)
    except EmptyPage:
        contacts = paginator.page(paginator.num_pages)
    return contacts

