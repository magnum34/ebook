# -*- coding: utf-8 -*-
from django.core.urlresolvers import resolve
from django.db.models import Count

from eBookApp.models.Book import Book

list_user = [
    {'url':'profil','label':'Profil','active':''},
    {'url':'profilAddress','label':'Dane Adresowe','active':''},
    {'url':'changePassword','label':'Zmiana hasła','active':''},
    {'url':'listOrders','label':'Lista zamówień','active':''},
    {'url':'signOut','label':'Wyloguj się','active':''}
]


def menu_categories(request):
    cats = Book.objects.all().values('category_id__name').annotate(total=Count('category_id__name'))
    cats = [ (item['category_id__name'], item['total']) for  item in cats]
    return {'menu':cats}

def menu_user(request):
    name_path = resolve(request.path_info).url_name
    for item in list_user:
        item['active'] = ''
        if name_path == item['url']:
            item['active'] = 'active'
    return {'menu_user':list_user}