#-*- coding: UTF-8 -*-
from decimal import Decimal

from eBookApp.models.Book import Book


class CartItem(object):
    def __init__(self,product,quantity,price):
        self.product = product
        self.quantity = int(quantity)
        self.price = Decimal(str(price))
    @property
    def dict(self):
        return {
            'produkt_id': self.product.pk,
            'quantity': self.quantity,
            'price': str(self.price)
        }
    def __repr__(self):
        return 'CartItem Object (%s)'% self.product
    def __unicode__(self):
        return "product = %s , quantity = %s , price = %s "%(self.product.id, self.quantity, self.price)
    @property
    def subtotal(self):
        return self.quantity *self.price

class Cart(object):
    def __init__(self,session,session_key='cart'):
        self._items_dict = {}
        self.session = session
        self.session_key = session_key
        if self.session_key in self.session:
            cart_session = self.session[self.session_key]
            products = self.get_queryset(cart_session.keys())
            for product in products:
                item = cart_session[str(product.pk)]
                self._items_dict[product.pk] = CartItem(product,item['quantity'],Decimal(item['price']))

    def get_queryset(self,keys):
        return Book.objects.filter(pk__in=keys)
    def add(self,product,price=None,quantity=1):
        quantity = int(quantity)
        if quantity < 1:
            raise ValueError('W koszu musi być przynajmniej jeden produkt')
        if product in self.products:
            self._items_dict[product.pk].quantity += quantity
        else:
            if price == None:
                raise ValueError('NIe podano ceny produktu !!!')
            self._items_dict[product.pk] = CartItem(product,quantity,price)

        self.update_session()
    def remove(self,product):
        if product in self.products:
            del self._items_dict[product.pk]
            self.update_session()
    def remove_signle(self,product):
        if product in self.products:
            if self._items_dict[product.pk].quantity <= 1:
                del self._items_dict[product.pk]
            else:
                self._items_dict[product.pk].quantity -= 1
            self.update_session()
    def update_session(self):
        self.session.modified = True
        self.session[self.session_key] = self.cart_serializable
    def clear(self):
        self._items_dict = {}
        self.update_session()
    @property
    def cart_serializable(self):
        cart_representation = {}
        for item in self.items:
            product_id = str(item.product.pk)
            cart_representation[product_id] = item.dict
        return cart_representation
    @property
    def items(self):
        return self._items_dict.values()
    @property
    def products(self):
        return [ item.product for item in self.items]
    @property
    def count(self):
        return sum([item.quantity for item in self.items])
    @property
    def total(self):
        return sum([item.subtotal for item in self.items])



