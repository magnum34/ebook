# eBook - config #
***  
## install virtualenv ##
link:    
 
* https://pip.pypa.io/en/stable/installing/       
* https://virtualenv.pypa.io/en/stable/installation/     

**
command install virtualenv (linux) and install environment:**     

```
#!python
 pip install virtualenv
 mkdir Django
 cd Django
 virtualenv env

```
**activate environment python/Django
**
```
#!python
 source env/bin/activate

```

Install Django and others modules and packages
----------------------------------------------
----
Requirements Files:

```
#!python

 pip install -r requirements.txt

```
Problem MySQL-python package:    
soluction:  

```
#!python

 sudo apt-get install libmysqlclient-dev
```
Config DataBase MySQL
----------------------------------------------
----
change settings.py in eBook project:


```
#!python


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'ebooks', #required create database name
        'USER':'root', #name superuser MySQL
        'PASSWORD':'root', 
    }
}
```
migrations Database to MySQL (required first connection MySQL):

```
#!python

python manage.py syncdb #synchronization database with Entity
```
Very change model/Entity and MySQL make command:

```
#!python

 python manage.py makemigrations NameApp
 python manage.py migrate # send change to server MySQL
```
Make staticfiles app
----------------------------------------------
----
   

```
#!python

 python manage.py collectstatic -l
```

Running server local for Django
----------------------------------------------
----

```
#!python

python manage.py runserver localhost:8000
```